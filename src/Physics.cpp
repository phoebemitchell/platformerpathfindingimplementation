//
// Created by Phoebe Mitchell on 17/02/2023.
//

#include "Physics.h"

const int VELOCITY_ITERATIONS = 18;
const int POSITION_ITERATIONS = 10;

Physics::Physics(float gravity, float timeStep) : m_gravity(0, gravity), m_world(m_gravity){
    m_timeStep = timeStep;
}

b2Body *Physics::CreateBody(b2BodyDef *bodyDef) {
    return m_world.CreateBody(bodyDef);
}

void Physics::Step() {
    m_world.Step(m_timeStep, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
}
