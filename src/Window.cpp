//
// Created by Phoebe Mitchell on 06/02/2023.
//

#include "Window.h"

Window::Window(int width, int height, std::string name) : m_renderWindow(sf::VideoMode(width, height), name){

}

Window::~Window() {

}

bool Window::IsOpen() {
    return m_renderWindow.isOpen();
}

void Window::PollEvents() {
    sf::Event event;
    while (m_renderWindow.pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            m_renderWindow.close();
            return;
        }

        for (auto eventAction : m_eventActions) {
            if (event.type == eventAction.eventType) {
                eventAction.action(event);
            }
        }
    }
}

void Window::Clear(sf::Color color) {
    m_renderWindow.clear(color);
}

void Window::Draw(Drawable *drawable) {
    drawable->DrawToWindow(&m_renderWindow);
}

void Window::Display() {
    m_renderWindow.display();
}

sf::Vector2i Window::GetMousePosition() {
    return sf::Mouse::getPosition(m_renderWindow);
}

void Window::AddEventAction(EventAction eventAction) {
    m_eventActions.push_back(eventAction);
}
