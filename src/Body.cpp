//
// Created by Phoebe Mitchell on 17/02/2023.
//

#include "Body.h"
#include <box2d/b2_fixture.h>
#include <iostream>

Body::Body(Physics *physics, float width, float height, float x, float y, float mass, b2BodyType bodyType,
           float friction) {
    m_bodyDef.type = bodyType;
    m_body = physics->CreateBody(&m_bodyDef);
    m_body->SetTransform(b2Vec2(x, y), 0);
    m_polygonShape.SetAsBox(width / 2, height / 2);
    if (bodyType == b2_dynamicBody) {
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &m_polygonShape;
        fixtureDef.density = mass / (width * height);
        fixtureDef.friction = friction;
        m_fixture = m_body->CreateFixture(&fixtureDef);
    } else {
        m_fixture = m_body->CreateFixture(&m_polygonShape, 0);
    }
}

void Body::SetCollision(bool enabled) {
    m_fixture->SetSensor(!enabled);
}

void Body::SetPosition(float x, float y) {
    m_body->SetAwake(true);
    m_body->SetTransform(b2Vec2(x, y), 0);
}

sf::Vector2f Body::GetPosition() {
    b2Vec2 position = m_body->GetPosition();
    return sf::Vector2f(position.x, position.y);
}
