//
// Created by Phoebe Mitchell on 06/02/2023.
//

#include <cmath>
#include <iostream>
#include "Grid.h"
#include "Body.h"

Grid::Grid(int width, int height, int pixelsPerUnit, Physics *physics) {
    m_width = width;
    m_height = height;
    m_tiles = std::make_unique<Tile[]>(width * height);
    m_pixelsPerUnit = pixelsPerUnit;
    srand(time(NULL));

    for (int i = 0; i < m_width * m_height; i++) {
        auto tile = &m_tiles.get()[i];
        tile->SetSize(pixelsPerUnit, pixelsPerUnit);
        tile->SetPosition((i % m_width) * pixelsPerUnit, floor(i / m_width) * pixelsPerUnit);
        auto body = tile->CreateBody(physics, pixelsPerUnit, pixelsPerUnit, 1, b2_staticBody, 0);
        body->SetCollision(false);
    }
}

Grid::~Grid() {

}

void Grid::DrawToWindow(sf::RenderWindow *renderWindow) {
    for (int i = 0; i < m_width * m_height; i++) {
        m_tiles.get()[i].DrawToWindow(renderWindow);
    }
}

Tile *Grid::GetTile(int x, int y) {
    if (x < 0 || x >= m_width || y < 0 || y >= m_height) {
        return nullptr;
    }

    return &m_tiles.get()[m_width * y + x];
}

sf::Vector2i Grid::ConvertPositionToTileCoordinate(sf::Vector2i position) {
    auto x = (float)position.x / (m_pixelsPerUnit * m_width) * m_width;
    auto y = (float)position.y / (m_pixelsPerUnit * m_height) * m_height;
    return {(int)x, (int)y};
}

int Grid::GetSize() {
    return m_width * m_height;
}

Tile *Grid::GetTileLinear(int index) {
    return &m_tiles.get()[index];
}

int Grid::GetWidth() {
    return m_width;
}

int Grid::GetHeight() {
    return m_height;
}

void Grid::Reset() {
    for (int i = 0; i < m_width * m_height; i++) {
        Tile *tile = &m_tiles.get()[i];
        if (tile->GetType() == TileType::Air) {
            tile->SetColor(AIR_TILE_COLOR);
        }
    }
}
