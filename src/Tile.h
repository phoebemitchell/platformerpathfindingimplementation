//
// Created by Phoebe Mitchell on 06/02/2023.
//

#ifndef PLATFORMERPATHFINDINGIMPLEMENTATION_TILE_H
#define PLATFORMERPATHFINDINGIMPLEMENTATION_TILE_H

#include <SFML/Graphics.hpp>
#include "Drawable.h"
#include "Updatable.h"
#include "Body.h"
#include "Physics.h"

enum class TileType {
    Air,
    Ground
};

const sf::Color GROUND_TILE_COLOR = sf::Color::Green;
const sf::Color AIR_TILE_COLOR = sf::Color::Transparent;

class Tile : public Drawable, public Updatable{
public:
    Tile();
    ~Tile();

    void SetSize(int width, int height);
    void SetPosition(int x, int y);
    sf::Vector2f GetPosition();
    void SetColor(sf::Color color);
    void DrawToWindow(sf::RenderWindow *renderWindow) override;
    void SetType(TileType tileType);
    TileType GetType();
    void Update() override;
    Body *CreateBody(Physics *physics, float width, float height, float mass, b2BodyType bodyType, float friction);
private:
    TileType m_tileType = TileType::Air;
    sf::RectangleShape m_rectangleShape;
    std::unique_ptr<Body> m_body;
};


#endif //PLATFORMERPATHFINDINGIMPLEMENTATION_TILE_H
