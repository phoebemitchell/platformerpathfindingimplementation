# Platformer Pathfinding Implementation
This is a visual front-end of the pathfinding library for 2D platformers found at https://gitlab.com/phoebemitchell/platformerpathfinding.

# Build Instructions
Use the following commands to build the project.
```
mkdir build
cd build
cmake ..
make
```

To run the program, use the following command.
```
./PlatformerPathfindingImplementation
```

# Instructions for use
- Press "1" to set the start point
- Press "2" to set the end point
- Use the left mouse button to draw terrain
- Use the right mouse button to remove terrain
- Press space to generate a path