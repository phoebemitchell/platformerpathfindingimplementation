set(FIND_PLATFORMERPATHFINDING_PATHS usr/local/Frameworks/PlatformerPathfinding)

find_path(PLATFORMERPATHFINDING_INCLUDE_DIR PlatformerPathfinding.h Node.h GraphLoading.h
        PATH_SUFFIXES include
        PATHS ${FIND_PLATFORMERPATHFINDING_PATHS})

find_library(PLATFORMERPATHFINDING_LIBRARY
        NAMES PlatformerPathfinding
        PATH_SUFFIXES lib
        PATHS ${FIND_PLATFORMERPATHFINDING_PATHS})