#include "src/Window.h"
#include "src/Grid.h"
#include "src/Repeater.h"
#include "src/Physics.h"
#include "src/Body.h"

const int WINDOW_WIDTH = 660*2;
const int WINDOW_HEIGHT = 420*2;
const std::string WINDOW_NAME = "Platformer Pathfinding Implementation";
const sf::Color WINDOW_CLEAR_COLOR = sf::Color::Cyan;

extern "C" {
    #include "pathfinding/PlatformerPathfinding.h"
    #include "pathfinding/GraphLoading.h"
}
#include "stdio.h"

std::string GenerateNavigationMesh(Grid *grid) {
    std::string output;
    for (int y = 0; y < grid->GetHeight(); y++) {
        for (int x = 0; x < grid->GetWidth(); x++) {
            Tile *tile = grid->GetTile(x, y);
            if (tile->GetType() == TileType::Ground) {
                output += GROUND_CHAR;
                continue;
            }

            if (tile->GetType() == TileType::Air) {
                Tile *belowTile = grid->GetTile(x, y + 1);
                if (belowTile != nullptr && belowTile->GetType() == TileType::Ground) {
                    output += TRAVERSABLE_CHAR;
                    continue;
                }

                output += AIR_CHAR;
            }
        }
        output += '\n';
    }
    output = output.substr(0, output.length() - 1);
    return output;
}

void SetTileType(Window *window, Grid *grid, TileType tileType) {
    auto mousePosition = window->GetMousePosition();
    auto gridCoordinate = grid->ConvertPositionToTileCoordinate(mousePosition);
    auto tile = grid->GetTile(gridCoordinate.x, gridCoordinate.y);
    if (tile == nullptr) {
        return;
    }
    tile->SetType(tileType);
}

int main() {
    Physics physics(10, 1.0f/60.0f);

    std::vector<Drawable*> drawables;
    std::vector<Updatable*> updatables;

    Window window(WINDOW_WIDTH, WINDOW_HEIGHT + 100, WINDOW_NAME);
    Grid grid(WINDOW_WIDTH / Physics::PIXELS_PER_UNIT, WINDOW_HEIGHT / Physics::PIXELS_PER_UNIT, Physics::PIXELS_PER_UNIT, &physics);
    drawables.push_back(&grid);

    Repeater repeaterLeftClick;
    updatables.push_back(&repeaterLeftClick);
    repeaterLeftClick.SetAction([&]() {
        SetTileType(&window, &grid, TileType::Ground);
    });

    Repeater repeaterRightClick;
    updatables.push_back(&repeaterRightClick);
    repeaterRightClick.SetAction([&]() {
        SetTileType(&window, &grid, TileType::Air);
    });

    EventAction leftPress;
    leftPress.eventType = sf::Event::MouseButtonPressed;
    leftPress.action = [&](const sf::Event event) {
        if (event.mouseButton.button != sf::Mouse::Button::Left) {
            return;
        }
        repeaterLeftClick.SetEnabled(true);
    };
    window.AddEventAction(leftPress);

    EventAction leftRelease;
    leftRelease.eventType = sf::Event::MouseButtonReleased;
    leftRelease.action = [&](const sf::Event event) {
        if (event.mouseButton.button != sf::Mouse::Button::Left) {
            return;
        }
        repeaterLeftClick.SetEnabled(false);
    };
    window.AddEventAction(leftRelease);

    EventAction rightPress;
    rightPress.eventType = sf::Event::MouseButtonPressed;
    rightPress.action = [&](const sf::Event event) {
        if (event.mouseButton.button != sf::Mouse::Button::Right) {
            return;
        }
        repeaterRightClick.SetEnabled(true);
    };
    window.AddEventAction(rightPress);

    EventAction rightRelease;
    rightRelease.eventType = sf::Event::MouseButtonReleased;
    rightRelease.action = [&](const sf::Event event) {
        if (event.mouseButton.button != sf::Mouse::Button::Right) {
            return;
        }
        repeaterRightClick.SetEnabled(false);
    };
    window.AddEventAction(rightRelease);

    Tile startTile;
    startTile.CreateBody(&physics, Physics::PIXELS_PER_UNIT, Physics::PIXELS_PER_UNIT, 1, b2_dynamicBody, 1);
    updatables.push_back(&startTile);
    drawables.push_back(&startTile);
    startTile.SetColor(sf::Color::Blue);
    startTile.SetPosition(0, 0);
    startTile.SetSize(Physics::PIXELS_PER_UNIT, Physics::PIXELS_PER_UNIT);

    Tile targetTile;
    drawables.push_back(&targetTile);
    targetTile.SetColor(sf::Color::Yellow);
    auto coordinate = grid.ConvertPositionToTileCoordinate(sf::Vector2i(WINDOW_WIDTH, WINDOW_HEIGHT));
    targetTile.SetPosition( (coordinate.x - 1) * Physics::PIXELS_PER_UNIT, (coordinate.y - 1) * Physics::PIXELS_PER_UNIT);
    targetTile.SetSize(Physics::PIXELS_PER_UNIT, Physics::PIXELS_PER_UNIT);

    EventAction space;
    space.eventType = sf::Event::KeyPressed;
    space.action = [&](const sf::Event event) {
        if (event.key.code != sf::Keyboard::Key::Space) {
            return;
        }

        grid.Reset();
        std::string mesh = GenerateNavigationMesh(&grid);
        FILE* file = fmemopen((void *) mesh.c_str(), mesh.length(), "r");
        NodeList nodeList;
        GraphLoading_ParseGraph(&nodeList, file);
        auto startPosition = grid.ConvertPositionToTileCoordinate((sf::Vector2i)startTile.GetPosition());
        auto targetPosition = grid.ConvertPositionToTileCoordinate((sf::Vector2i)targetTile.GetPosition());
        NodeIDList nodeIDList = PlatformerPathfinding_FindPath(&nodeList, startPosition.y * grid.GetWidth() + startPosition.x, targetPosition.y * grid.GetWidth() + targetPosition.x, Platformer, 5);

        for (int i = 0; i < nodeIDList.count; i++) {
            Tile *tile = grid.GetTileLinear(nodeIDList.nodeIDs[i]);
            if (tile != nullptr) {
                tile->SetColor(sf::Color::Red);
            }
        }

        Node_FreeNodeList(&nodeList);
        Node_FreeNodeIDList(&nodeIDList);
    };
    window.AddEventAction(space);

    EventAction onePressed;
    onePressed.eventType = sf::Event::KeyPressed;
    onePressed.action = [&](sf::Event event) {
        if (event.key.code != sf::Keyboard::Num1) {
            return;
        }
        auto gridPosition = grid.ConvertPositionToTileCoordinate(window.GetMousePosition());
        startTile.SetPosition(gridPosition.x * Physics::PIXELS_PER_UNIT, gridPosition.y * Physics::PIXELS_PER_UNIT);
    };
    window.AddEventAction(onePressed);

    EventAction twoPressed;
    twoPressed.eventType = sf::Event::KeyPressed;
    twoPressed.action = [&](sf::Event event) {
        if (event.key.code != sf::Keyboard::Num2) {
            return;
        }
        auto gridPosition = grid.ConvertPositionToTileCoordinate(window.GetMousePosition());
        targetTile.SetPosition(gridPosition.x * Physics::PIXELS_PER_UNIT, gridPosition.y * Physics::PIXELS_PER_UNIT);
    };
    window.AddEventAction(twoPressed);

    while (window.IsOpen()) {
        window.PollEvents();
        window.Clear(WINDOW_CLEAR_COLOR);
        for (auto updatable : updatables) {
            updatable->Update();
        }
        for (auto drawable : drawables) {
            window.Draw(drawable);
        }
        window.Display();
    }
    return 0;
}
