//
// Created by Phoebe Mitchell on 06/02/2023.
//

#ifndef PLATFORMERPATHFINDINGIMPLEMENTATION_GRID_H
#define PLATFORMERPATHFINDINGIMPLEMENTATION_GRID_H

#include "Drawable.h"
#include "Tile.h"
#include "Updatable.h"

class Grid : public Drawable {
public:
    Grid(int width, int height, int pixelsPerUnit, Physics *physics);
    ~Grid();

    void DrawToWindow(sf::RenderWindow *renderWindow) override;
    Tile *GetTile(int x, int y);
    sf::Vector2i ConvertPositionToTileCoordinate(sf::Vector2i position);
    void Reset();
    int GetSize();
    int GetWidth();
    int GetHeight();
    Tile *GetTileLinear(int index);
private:
    std::unique_ptr<Tile[]> m_tiles;

    int m_pixelsPerUnit;
    int m_width;
    int m_height;
};


#endif //PLATFORMERPATHFINDINGIMPLEMENTATION_GRID_H
