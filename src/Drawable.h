//
// Created by Phoebe Mitchell on 06/02/2023.
//

#ifndef PLATFORMERPATHFINDINGIMPLEMENTATION_DRAWABLE_H
#define PLATFORMERPATHFINDINGIMPLEMENTATION_DRAWABLE_H


#include <SFML/Graphics.hpp>

class Drawable {
public:
    virtual void DrawToWindow(sf::RenderWindow *renderWindow) = 0;
};


#endif //PLATFORMERPATHFINDINGIMPLEMENTATION_DRAWABLE_H
