//
// Created by Phoebe Mitchell on 02/02/2023.
//

#include <stdlib.h>
#include "Node.h"

// Allocate memory for a node ID list and initialise its count to 0
NodeIDList Node_CreateNodeIDList() {
    NodeIDList nodeIDList;
    nodeIDList.nodeIDs = malloc(0);
    nodeIDList.count = 0;
    return nodeIDList;
}

// Resizes a NodeIDList and adds a new node ID to it
void Node_AddToNodeIdList(NodeIDList *nodeIDList, int nodeID) {
    nodeIDList->count++;
    nodeIDList->nodeIDs = realloc(nodeIDList->nodeIDs, sizeof(int) * nodeIDList->count);
    nodeIDList->nodeIDs[nodeIDList->count - 1] = nodeID;
}

// Removes a node ID from a node ID list
void Node_RemoveFromNodeIDList(NodeIDList *nodeIDList, int nodeID) {
    int position = 0;
    for (int i = 0; i < nodeIDList->count; i++) {
        if (nodeIDList->nodeIDs[i] == nodeID) {
            position = i;
            break;
        }
    }
    for (int i = position; i < nodeIDList->count - 1; i++) {
        nodeIDList->nodeIDs[i] = nodeIDList->nodeIDs[i + 1];
    }
    nodeIDList->count--;
    nodeIDList->nodeIDs = realloc(nodeIDList->nodeIDs, sizeof(int) * nodeIDList->count);
}

// Checks a node ID list to see if it contains a given node ID
// Returns the position of the node in the list or -1 if node is not in the list
char Node_NodeIDListContains(NodeIDList *nodeIDList, int nodeID) {
    for (int i = 0; i < nodeIDList->count; i++) {
        if (nodeID == nodeIDList->nodeIDs[i]) {
            return i;
        }
    }
    return -1;
}

// Reverses the order of node IDs in a node ID list
void Node_ReverseNodeIdList(NodeIDList *nodeIdList) {
    for (int i = 0; i < nodeIdList->count / 2; i++) {
        int temp = nodeIdList->nodeIDs[nodeIdList->count - 1 - i];
        nodeIdList->nodeIDs[nodeIdList->count - 1 - i] = nodeIdList->nodeIDs[i];
        nodeIdList->nodeIDs[i] = temp;
    }
}

// Allocates memory for a connects list in a node
void Node_CreateConnectionsList(Node *node) {
    node->connections = malloc(0);
    node->connectionCount = 0;
}

// Adds a connection to a node
void Node_AddConnection(Node *node, int connectionId, float cost, MovementType movementType) {
    node->connectionCount++;
    node->connections = realloc(node->connections, sizeof(Connection) * node->connectionCount);
    node->connections[node->connectionCount - 1].nodeId = connectionId;
    node->connections[node->connectionCount - 1].cost = cost;
    node->connections[node->connectionCount - 1].movementType = movementType;
}

// Creates a node list
void Node_InitialiseNodeList(NodeList *nodeList) {
    nodeList->nodes = malloc(0);
    nodeList->count = 0;
}

// Adds a new node to a node list and returns its address
Node *Node_AddNewNodeToNodeList(NodeList *nodeList) {
    nodeList->count++;
    nodeList->nodes = realloc(nodeList->nodes, sizeof(Node) * nodeList->count);
    nodeList->nodes[nodeList->count - 1].heapPosition = -1;
    return &nodeList->nodes[nodeList->count - 1];
}

void Node_FreeNodeList(NodeList *nodeList) {
    for (int i = 0; i < nodeList->count; i++) {
        free(nodeList->nodes[i].connections);
    }
    nodeList->count = 0;
    free(nodeList->nodes);
}

void Node_FreeNodeIDList(NodeIDList *nodeIdList) {
    free(nodeIdList->nodeIDs);
}
