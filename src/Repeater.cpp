//
// Created by Phoebe Mitchell on 06/02/2023.
//

#include "Repeater.h"

void Repeater::Update() {
    if (!m_enabled) {
        return;
    }

    m_action();
}

void Repeater::SetEnabled(bool enabled) {
    m_enabled = enabled;
}

void Repeater::SetAction(std::function<void()> action) {
    m_action = action;
}
