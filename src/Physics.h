//
// Created by Phoebe Mitchell on 17/02/2023.
//

#ifndef PLATFORMERPATHFINDINGIMPLEMENTATION_PHYSICS_H
#define PLATFORMERPATHFINDINGIMPLEMENTATION_PHYSICS_H

#include <box2d/b2_world.h>

class Physics {
public:
    static const int PIXELS_PER_UNIT = 30;

    Physics(float gravity, float timeStep);

    void Step();
    b2Body *CreateBody(b2BodyDef *bodyDef);
private:
    b2Vec2 m_gravity;
    b2World m_world;
    float m_timeStep;
};


#endif //PLATFORMERPATHFINDINGIMPLEMENTATION_PHYSICS_H
