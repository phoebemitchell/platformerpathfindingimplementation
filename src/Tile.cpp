//
// Created by Phoebe Mitchell on 06/02/2023.
//

#include <iostream>
#include "Tile.h"
#include <box2d/b2_contact.h>

Tile::Tile() : m_rectangleShape(sf::Vector2f(0, 0)){
    m_rectangleShape.setFillColor(AIR_TILE_COLOR);
}

Tile::~Tile() {

}

void Tile::DrawToWindow(sf::RenderWindow *renderWindow) {
    renderWindow->draw(m_rectangleShape);
}

void Tile::SetSize(int width, int height) {
    m_rectangleShape.setSize(sf::Vector2f(width, height));
}

void Tile::SetPosition(int x, int y) {
    if (m_body != nullptr) {
        m_body->SetPosition(x, y);
        return;
    }
    m_rectangleShape.setPosition(x, y);
}

void Tile::SetColor(sf::Color color) {
    m_rectangleShape.setFillColor(color);
}

void Tile::SetType(TileType tileType) {
    sf::Color color = tileType == TileType::Air ? AIR_TILE_COLOR : GROUND_TILE_COLOR;
    m_body->SetCollision(tileType == TileType::Ground);
    SetColor(color);
    m_tileType = tileType;
}

TileType Tile::GetType() {
    return m_tileType;
}

sf::Vector2f Tile::GetPosition() {
    return m_rectangleShape.getPosition();
}

void Tile::Update() {
    if (m_body == nullptr) {
        return;
    }
    m_rectangleShape.setPosition(m_body->GetPosition());
}

Body *Tile::CreateBody(Physics *physics, float width, float height, float mass, b2BodyType bodyType, float friction) {
    m_body = std::make_unique<Body>(physics, width, height, GetPosition().x, GetPosition().y, mass, bodyType, friction);
    return m_body.get();
}
