//
// Created by Phoebe Mitchell on 17/02/2023.
//

#ifndef PLATFORMERPATHFINDINGIMPLEMENTATION_BODY_H
#define PLATFORMERPATHFINDINGIMPLEMENTATION_BODY_H

#include "Physics.h"
#include <box2d/b2_body.h>
#include <box2d/b2_polygon_shape.h>
#include <box2d/b2_fixture.h>
#include <SFML/Graphics/Vertex.hpp>

class Body {
public:
    Body(Physics *physics, float width, float height, float x, float y, float mass, b2BodyType bodyType,
         float friction);

    void SetPosition(float x, float y);
    sf::Vector2f GetPosition();
    void SetCollision(bool enabled);
private:
    b2BodyDef m_bodyDef;
    b2Body *m_body;
    b2PolygonShape m_polygonShape;
    b2Fixture *m_fixture;
};


#endif //PLATFORMERPATHFINDINGIMPLEMENTATION_BODY_H
