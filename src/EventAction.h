//
// Created by Phoebe Mitchell on 06/02/2023.
//

#ifndef PLATFORMERPATHFINDINGIMPLEMENTATION_EVENTACTION_H
#define PLATFORMERPATHFINDINGIMPLEMENTATION_EVENTACTION_H

#include <functional>
#include <SFML/Window.hpp>

struct EventAction {
    sf::Event::EventType eventType;
    std::function<const void(const sf::Event)> action;
};

#endif //PLATFORMERPATHFINDINGIMPLEMENTATION_EVENTACTION_H
