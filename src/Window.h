//
// Created by Phoebe Mitchell on 06/02/2023.
//

#ifndef PLATFORMERPATHFINDINGIMPLEMENTATION_WINDOW_H
#define PLATFORMERPATHFINDINGIMPLEMENTATION_WINDOW_H

#include <string>
#include <SFML/Graphics.hpp>
#include "Drawable.h"
#include "EventAction.h"

class Window {
public:
    Window(int width, int height, std::string name);
    ~Window();

    bool IsOpen();
    void PollEvents();
    void Clear(sf::Color color);
    void Draw(Drawable *drawable);
    void Display();
    sf::Vector2i GetMousePosition();
    void AddEventAction(EventAction eventAction);
private:
    sf::RenderWindow m_renderWindow;
    std::vector<EventAction> m_eventActions;
};


#endif //PLATFORMERPATHFINDINGIMPLEMENTATION_WINDOW_H
