//
// Created by Phoebe Mitchell on 06/02/2023.
//

#ifndef PLATFORMERPATHFINDINGIMPLEMENTATION_REPEATER_H
#define PLATFORMERPATHFINDINGIMPLEMENTATION_REPEATER_H


#include <functional>
#include "Updatable.h"

class Repeater : public Updatable {
public:
    void Update() override;
    void SetEnabled(bool enabled);
    void SetAction(std::function<void()> action);
private:
    bool m_enabled = false;
    std::function<void()> m_action;
};


#endif //PLATFORMERPATHFINDINGIMPLEMENTATION_REPEATER_H
